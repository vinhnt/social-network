$(document).ready(function() {
    $("#save").click(function() {
        $("form[name='editprofile']").validate({
            rules: {
                name: {
                    required: true
                }
            },

            //  ***  Validation error messages  ***
            messages: {
                name: "Please enter a valid name",
            },

            submitHandler: function () {
                var name = $("#name").val();
                // var photo = $("#photo").val();
                $.ajax({
                    type: "POST",
                    url: "http://127.0.0.1:8000/api/update/profile",
                    data: JSON.stringify({
                        name: name,
                        image_avatar: photo
                    }),
                    contentType: "Application/json",
                    dataType: "json",
                    success: function (data) {
                        console.log("the success function", data);
                    },
                    error: function (req, status, err) {
                        console.log(
                            "Oops, something went wrong!!!",
                            status,
                            err
                        );
                    },
                });
            },
        });
    });
});