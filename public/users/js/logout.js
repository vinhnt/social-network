$(document).ready(function() {
   $("#logout").click(function(event) {
    event.preventDefault();
    var token = localStorage.getItem("token");
            $.ajax({
                method: "POST",
                url: "http://127.0.0.1:8000/api/logout",
                data: {},
                contentType: "Application/json",
                dataType: "json",
                beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer '+ token);
            },
            success: function (data) {
                window.localStorage.clear();
                window.location.replace("http://127.0.0.1:8000/user/login");
            },
            error: function (req, status, err) {
                console.log(
                    "Oops, something went wrong!!!",
                    status,
                    err
                );
            },
        });
   });
});