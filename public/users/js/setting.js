$(document).ready(function() {
    var token = localStorage.getItem("token");
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:8000/api/user/setting",
        contentType: "Application/json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+ token);
        },
        success: function (data) {
            document.querySelector(".user-profile").innerHTML = data;
        },
        error: function (req, status, err) {
            console.log(
                "Oops, something went wrong!!!",
                status,
                err
            );
        },
    });
});