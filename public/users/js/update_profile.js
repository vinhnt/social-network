$(document).ready(function(){
    var token = localStorage.getItem("token");
    $("#save").click(async function  (event) {
        event.preventDefault();
        var formData = new FormData();
        var files = $('#fileUpload1')[0].files;
        formData.append('fileOne',$('#fileUpload1')[0].files[0]);
        formData.append('name',$("#name").val()||"test");
        $.ajax({
            type: "POST",
            url: "http://127.0.0.1:8000/api/update/profile",
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer '+ token);
            },
            success: function (data) {
                alert(data.message);
            },
            error: function (req, status, err) {
                console.log(
                    "Oops, something went wrong!!!",
                    status,
                    err
                );
            },
        });
    });
});