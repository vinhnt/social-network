$(document).ready(function() {
    var token = localStorage.getItem("token");
    $.ajax({
        method: "GET",
        url: "http://127.0.0.1:8000/api/list/photo",
        data: {},
        contentType: "Application/json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+ token);
        },
        success: function(data) {
            document.querySelector('.list_photo').innerHTML = data;
        },
        error: function (req, status, err) {
            console.log(
                "Oops, something went wrong!!!",
                status,
                err
            );
        },           
    });
});