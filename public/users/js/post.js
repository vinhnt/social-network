$(document).ready(function() {
    var token = localStorage.getItem("token");
    $("body").on('click', '.post-btn', function() {
        $("form[name='addpost']").validate({
            rules: {
                content_post: {
                    required: true,
                },
            },

            //  ***  Validation error messages  ***

            messages: {
                content_post: {
                    required: "Please typing content post",
                },
            },

            submitHandler: function () {
                var content = $('#content_post').val();
                var status = 1;
                $.ajax({
                    type: "POST",
                    url: "http://127.0.0.1:8000/api/post/create",
                    data: JSON.stringify({
                        content : content,
                        status: status
                    }),
                    contentType: "Application/json",
                    dataType: "json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer '+ token);
                    },
                    success: function (data) {
                        console.log(data)
                        $('#content_post').val("");
                        alert (JSON.stringify(data.message));
                       $.ajax({
                            type: "GET",
                            url: "http://127.0.0.1:8000/api/post/list",
                            data: {},
                            contentType: "Application/json",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', 'Bearer '+ token);
                            },
                            success: function (data) {
                                document.querySelector(".user-post").innerHTML= data;
                            },
                            error: function (req, status, err) {
                                console.log(
                                    "Oops, something went wrong!!!",
                                    status,
                                    err
                                );
                            },
                       });
                    },
                    error: function (req, status, err) {
                        console.log(
                            "Oops, something went wrong!!!",
                            status,
                            err
                        );
                    },
                });
            },
        });
    });
});