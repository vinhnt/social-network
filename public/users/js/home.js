$(document).ready(function() {
    var token = localStorage.getItem("token");
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:8000/api/post/list",
        contentType: "Application/json",
        // dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+ token);
        },
        success: function (data) {    
            document.querySelector(".user-post").innerHTML= data;
        },
        error: function (req, status, err) {
            console.log(
                "Oops, something went wrong!!!",
                status,
                err
            );
        },
    });
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:8000/api/user/infor",
        contentType: "Application/json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+ token);
        },
        success: function (data) {
            document.querySelector(".new-postbox").innerHTML = data;
        },
        error: function (req, status, err) {
            console.log(
                "Oops, something went wrong!!!",
                status,
                err
            );
        },
    });
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:8000/api/user/current",
        contentType: "Application/json",
        beforeSend:function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+ token);
        },
        success: function (data) {
            document.querySelector(".user-img").innerHTML = data;
        },
        error: function(req, status, err) {
            console.log(
                "Oops, something went wrong !!!",
                status,
                err
            );
        }
    });
});

    



