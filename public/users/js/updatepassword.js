$(document).ready(function() {
    $("#btn_changepassword").click(function() {
        $("form[name='changepassword']").validate({
            rules: {
                uuid: {
                    required: true,
                },
                password: {
                    required: true,
                },
                repassword: {
                    required: true,
                    equalTo: "#password",
                },
            },

            //  ***  Validation error messages  ***

            messages: {
                uuid: "Please enter a valid uuid",

                password: {
                    required: "Please provide a password",
                },

                repassword: {
                    required: "Please provide your password",
                    equalTo: "Your password must equal your first password",
                },
            },

            submitHandler: function () {
                var uuid = $("#uuid").val();
                var password = $("#password").val();
                var repassword = $("#repassword").val();
                $.ajax({
                    type: "POST",
                    url: "http://127.0.0.1:8000/api/update/password",
                    data: JSON.stringify({
                        uuid: uuid,
                        password: password,
                        repassword: repassword,
                    }),
                    contentType: "Application/json",
                    dataType: "json",
                    success: function (data) {
                        console.log("the success function", data);
                    },
                    error: function (req, status, err) {
                        console.log(
                            "Oops, something went wrong!!!",
                            status,
                            err
                        );
                    },
                });
            },
        });
    });
});