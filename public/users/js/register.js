$(document).ready(function () {
    $("#btn_register").click(function () {
        $("form[name='registration']").validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                },
                repassword: {
                    required: true,
                    equalTo: "#password",
                },
            },

            //  ***  Validation error messages  ***

            messages: {
                name: "Please enter a valid name",
                email: "Please enter a valid email address",

                password: {
                    required: "Please provide a password",
                },

                repassword: {
                    required: "Please provide your password",
                    equalTo: "Your password must equal your first password",
                },
            },

            submitHandler: function () {
                var name = $("#name").val();
                var email = $("#email").val();
                var password = $("#password").val();
                $.ajax({
                    type: "POST",
                    url: "http://127.0.0.1:8000/api/register",
                    data: JSON.stringify({
                        name: name,
                        email: email,
                        password: password,
                    }),
                    contentType: "Application/json",
                    dataType: "json",
                    success: function (data) {
                        console.log("the success function", data);
                    },
                    error: function (req, status, err) {
                        console.log(
                            "Oops, something went wrong!!!",
                            status,
                            err
                        );
                    },
                });
            },
        });
    });
});
