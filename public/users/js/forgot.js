
$(document).ready(function() {
    $("#btn_forgot").click(function() {
        $("form[name='forgotration']").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
            },

            //  ***  Validation error messages  ***

            messages: {
                email: "Please enter a valid email address"
            },

            submitHandler: function () {
                var email = $("#email").val();
                $.ajax({
                    type: "POST",
                    url: "http://127.0.0.1:8000/api/forgot/password",
                    data: JSON.stringify({
                        email: email,
                    }),
                    contentType: "Application/json",
                    dataType: "json",
                    success: function (data) {
                        console.log("the success function", data);
                    },
                    error: function (req, status, err) {
                        console.log(
                            "Oops, something went wrong!!!",
                            status,
                            err
                        );
                    },
                });
            },
        });
    });
});