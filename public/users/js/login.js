$(document).ready(function() {
    $("#btn_login").click(function() {
        $("form[name='formlogin']").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                },
            },

            //  ***  Validation error messages  ***

            messages: {
                email: "Please enter a valid email address",

                password: {
                    required: "Please provide a password",
                },
            },

            submitHandler: function () {
                var email = $("#email").val();
                var password = $("#password").val();
                $.ajax({
                    type: "POST",
                    url: "http://127.0.0.1:8000/api/login",
                    data: JSON.stringify({
                        email: email,
                        password: password,
                    }),
                    contentType: "Application/json",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        localStorage.setItem('token',data.authorisation.token);
                        window.location.replace("http://127.0.0.1:8000/user/index");
                    },
                    error: function (req, status, err) {
                        alert("Email or Password is not correct. Please try it again");
                    },
                });
            },
        });
    });
});