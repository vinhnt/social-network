
$(document).ready(function() {
    $('body').on('keypress', ".comment-post", function(event) {
       var keyCode = (event.keyCode ? event.keyCode : event.which)
       if(keyCode == '13') {
        var nodeCurrent = $(this);
        var postId = $(this).attr('postId');
        var content = $(this).val();
        var token = localStorage.getItem("token");
            $.ajax({
                type: "POST",
                url: "http://127.0.0.1:8000/api/comments/create",
                contentType: "Application/json",
                data: JSON.stringify({
                    content : content,
                    post_id : postId
                }),
                dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer '+ token);
                },
                success: function (data) {
                    var oldContent = data.comments.content;
                    var id = data.comments.user_id;
                    nodeCurrent.val("");
                    $.ajax({
                        type: "POST",
                        url: "http://127.0.0.1:8000/api/user/by/id",
                        contentType: "Application/json",
                        data: JSON.stringify({
                           id : id
                        }),
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer '+ token);
                        },
                        success: function (data) {
                           data[0].content = oldContent;
                           var htmls = data.map(function(item){
                                return `<li>
                                <div class="comet-avatar">
                                     <img src="http://127.0.0.1:8000/users/images/resources/${item.image_avatar}" alt="">
                                </div>
                                <div class="we-comment">
                                    <h5><a href="time-line.html" title="">${item.name}</a></h5>
                                    <p>${item.content}</p>
                                </div
                            </li>`;
                           });
                           var parentNode = nodeCurrent.parent().parent().parent().parent();
                           var lastParentNode = parentNode.children().last()
                           parentNode.append(htmls.join('')).before(lastParentNode);
                           parentNode.append(lastParentNode);
                        },
                        error: function (req, status, err) {
                            console.log(
                                "Oops, something went wrong!!!",
                                status,
                                err
                            );
                        },
                    });
                },
                error: function (req, status, err) {
                    console.log(
                        "Oops, something went wrong!!!",
                        status,
                        err
                    );
                },
            });
       }
    });

});