<?php

namespace App\Services;

use App\Services\GeneralService;

interface PhotoServiceInterface extends GeneralService
{
    public function index();
}
