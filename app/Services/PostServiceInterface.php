<?php

namespace App\Services;

use App\Services\GeneralService;

interface PostServiceInterface extends GeneralService
{
    public function create($data);
    public function delete($id);
    public function update($data, $id);
    public function list();
    public function allPost();
    public function getUser();
}
