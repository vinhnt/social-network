<?php

namespace App\Services;

use App\Services\UserServiceInterface;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Str;
use App\Jobs\SendMailForgotPassword;

class UserService implements UserServiceInterface
{
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

///LOGIN
    public function login($request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validator fails',
                'validate'=>$validator->messages(),
            ], 400);
        }

        $credentials = $request->only('email', 'password');
        $token = Auth::guard('api')->attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized',
            ], 401);
        }
        $user = Auth::guard('api')->user();
        return response()->json([
                'status' => 'success',
                'message' => 'Login successfully',
                'user' => $user,
                'authorisation' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ]);
    }
///REGISTER
    public function register($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:App\Models\User,email',
            'password' => 'required|string|min:8|max:32',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validator fails',
                'validate'=>$validator->messages(),
            ], 400);
        }

        try {
            $user = $this->userRepository->creatUser([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            //Auth::guard('api')->login($user);
            return response()->json([
                'status' => 'success',
                'message' => 'User created successfully',
                'user' => $user,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => 'Can\'t create user now',
            ], 409);
        }
    }
///LOGOUT
    public function logout()
    {
        Auth::guard('api')->logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out',
        ]);
    }
///REFRESH
    public function refresh()
    {
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully refresh token',
            'user' => Auth::guard('api')->user(),
            'authorisation' => [
                'token' => Auth::guard('api')->refresh(),
                'type' => 'bearer',
            ]
        ]);
    }
///
    public function getAll()
    {
        return $this->userRepository->getAll();
    }

    public function updateUserProfile($request)
    {
        $validator = Validator::make($request->all(), [
            'fileOne' => 'nullable|image|mimes:jpg,png,jpeg'
        ]);
        if ($validator->fails()) {
            return [
                'message' => 'Validator fails',
                'error' => $validator->errors(),
                'status' => 422
            ];
        }
        if ($request->hasfile('fileOne')) {
            $file=$request->file('fileOne');
            $filename=date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('users/images/resources'), $filename);
            return $this->userRepository->updateUserProfileWithImage($filename, $request->name);
        }
        return $this->userRepository->updateUserProfileWithoutImage($request->name);
    }

    public function changePassword($data)
    {
        $validator = Validator::make($data, [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);
        if ($validator->fails()) {
            return [
                'message' => 'Validator fails',
                'error' => $validator->errors(),
                'status' => 422
            ];
        }
        return $this->userRepository->changePassword($data);
    }

    public function creatUser($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string',
        ], [
            'name'=>'khong duoc de trong, phai la string',
            'email'=>'khong duoc de trong, phai la email',
            'password' => 'phai la string, khong trung ',
        ]);
        if ($validator->fails()) {
            return [
                'message' => 'Validator fails',
                'validate' => $validator,
            ];
        }
    }

    public function forgetPassword($data)
    {
        $validator = Validator::make($data, [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return [
                'message'=>'Validator fails',
                'error' => $validator->errors(),
                'status' => 422
            ];
        }
        $this->userRepository->checkEmail($data['email']);
        $uuid = Str::uuid();
        $this->userRepository->updateUuid($uuid, $data['email']);
        $details = [
                'to' => $data['email'],
                'uuid'=> $uuid,
                'link' => "http://127.0.0.1:8000/user/update/password"
            ];
        // return $details;
        $emailJob = new SendMailForgotPassword($details);
        // return $emailJob;
        dispatch($emailJob);
        return [
                'message' => 'Please check your email to change password'
            ];
    }

    public function updatePassword($data)
    {
        $validator = Validator::make($data, [
            'uuid' => 'required|string',
            'password' => 'required',
            'repassword' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return [
                'message'=>'Validator fails',
                'error' => $validator->errors(),
                'status' => 422
            ];
        }
        if (empty($this->userRepository->checkUuid($data['uuid']))) {
            return [
                'message' => 'Code is not correct.Please try it again'
            ];
        }
        return $this->userRepository->updatePassword($data);
    }

    public function infor()
    {
        return $this->userRepository->infor();
    }

    public function getUserById($data)
    {
        $validator = Validator::make($data, [
            'id'=> 'required'
        ]);
        if ($validator->fails()) {
            return [
                'message'=>'Validator fails',
                'error' => $validator->errors(),
                'status' => 422
            ];
        }
        return $this->userRepository->getUserById($data);
    }

    public function delete($id)
    {
        $this->userRepository->delete($id);
    }

    public function edit($id)
    {
        return $this->userRepository->edit($id);
    }
}
