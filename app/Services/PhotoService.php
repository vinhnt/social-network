<?php

namespace App\Services;

use App\Services\PhotoServiceInterface;
use App\Repositories\PhotoRepository;

class PhotoService implements PhotoServiceInterface
{
    public $photoRepository;
    public function __construct(PhotoRepository $photoRepository)
    {
        $this->photoRepository = $photoRepository;
    }

    public function index()
    {
        return $this->photoRepository->index();
    }
}
