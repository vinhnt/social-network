<?php

namespace App\Services;

use App\Services\CommentServiceInterface;
use App\Repositories\CommentRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentService implements CommentServiceInterface
{
    private $commentRepository;
    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }
///GET
    public function getByPostId($request)
    {
        $validator = Validator::make($request->route()->parameters(), [
            'id' => 'required|numeric',
        ], [
            'id.required'=>'khong duoc de trong',
            'id.numeric'=> 'id phai la so'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validate error',
                'validate'=>$validator->customMessages
            ]);
        }

        $comments = $this->commentRepository->getByPostId($request->route('id'));
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get all comment by post ID',
            'comments' => $comments
        ]);
    }
///EDIT
    public function editComment($request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required|numeric',
            'content'=> 'required',
        ], [
            'post_id.required'=>'khong duoc de trong',
            'post_id.numeric'=> 'id phai la so',
            'content.required'=>'khong duoc de trong',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validate error',
                'validate'=>$validator->customMessages
            ]);
        }
        $comments = $this->commentRepository->editComment($request);
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully edit  comment by id post',
            'comments' => $comments
        ]);
    }
///DELETE
    public function deleteComment($request)
    {
        $validator = Validator::make($request->route()->parameters(), [
            'id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validate fails',
                'validate'=>$validator->messages(),
            ]);
        }

        $comments = $this->commentRepository->deleteComment($request->route('id'));
        if (!$comments) {
            return response()->json([
                'status' => 'error',
                'message' => "Sorry, can't delete comment by id",
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully delete  comment by id comment',
            'comments' => $comments
        ]);
    }
///CREATE
    public function createComment($data)
    {
        $validator = Validator::make($data->all(), [
            'post_id' => 'required|numeric',
            'content' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validate param error',
                'validate'=>$validator->messages(),
            ]);
        }

        try {
            $comments = $this->commentRepository->create([
                'user_id' => auth('api')->user()->id,
                'post_id' => $data->post_id,
                'content' => $data->content,
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Successfully create  comment by id_user and id_post',
                'comments' => $comments
            ]);
        } catch (\Throwable $th) {
             return response()->json([
                'status' => 'error',
                'message' => 'Sorry! can\'t create comment now',
             ]);
        }
    }
}
