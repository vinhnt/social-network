<?php

namespace App\Services;

use App\Services\GeneralService;

interface CommentServiceInterface extends GeneralService
{
    public function getByPostId($request);
    public function editComment($request);
    public function deleteComment($request);
    public function createComment($data);
}
