<?php

namespace App\Services;

use App\Services\GeneralService;

interface LikeServiceInterface extends GeneralService
{
    public function getAll();
}
