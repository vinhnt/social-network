<?php

namespace App\Services;

use App\Models\Post;
use App\Services\PostServiceInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use App\Repositories\ListFriendRepository;

class PostService implements PostServiceInterface
{
    public $postRepository;
    public $userRepository;
    public $listFriendRepository;
    public function __construct(
        PostRepository $postRepository,
        UserRepository $userRepository,
        ListFriendRepository $listFriendRepository
    ) {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
        $this->listFriendRepository = $listFriendRepository;
    }
    public function create($data)
    {
        $validator = Validator::make($data, [
            'content' => 'required',
            'status' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return [
                'message' => 'Validator fails',
                'error' => $validator->errors(),
                'status' => 422
            ];
        }
        return $this->postRepository->create($data);
    }

    public function delete($id)
    {
        if (empty($this->postRepository->find($id))) {
            return [
                'message' => "This post doesn't exist in system",
                'status' => 422
            ];
        } elseif ($this->postRepository->postOfUser($id) == false) {
            return [
                'message' => "You are not author this post couldn't delete this post",
                'status' => 422
            ];
        } else {
            return $this->postRepository->delete($id);
        }
    }

    public function update($data, $id)
    {
        $validator = Validator::make($data, [
            'content' => 'required',
            'status' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return [
                'message' => 'Validator fails',
                'error' => $validator->errors(),
                'status' => 422
            ];
        }
        if (empty($this->postRepository->find($id))) {
            return [
                'message' => "This post doesn't exist in system",
                'status' => 422
            ];
        } elseif ($this->postRepository->postOfUser($id) == false) {
            return [
                'message' => "You are not author this post couldn't update this post",
                'status' => 422
            ];
        } else {
            return $this->postRepository->update($data, $id);
        }
    }

    public function list()
    {
        $id = $this->userRepository->user();
        $listFriends = $this->listFriendRepository->listFriend($id);
        $userIds =[];
        foreach ($listFriends as $key => $item) {
            array_push($userIds, $item->friend_id);
        }
        return $this->postRepository->list($userIds);
    }

    public function allPost()
    {
        $post =  $this->postRepository->all();
        return $post;
    }

    public function getUser()
    {
        return $this->userRepository->getUser();
    }
}
