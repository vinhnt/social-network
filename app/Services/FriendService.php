<?php

namespace App\Services;

use App\Services\FriendServiceInterface;
use App\Repositories\ListFriendRepository;
use App\Repositories\UserRepository;

class FriendService implements FriendServiceInterface
{
    private $listFriendRepository;
    private $userRepository;
    public function __construct(ListFriendRepository $listFriendRepository, UserRepository $userRepository)
    {
        $this->listFriendRepository = $listFriendRepository;
        $this->userRepository = $userRepository;
    }
    public function delete($id)
    {
        if ($this->listFriendRepository->isFriend($id)) {
            return [
                'message' => "Not friend you couldn't not unfriend.Please make friend",
                'status' => 422
            ];
        }
        return $this->listFriendRepository->delete($id);
    }

    public function list()
    {
        $listFriends = $this->listFriendRepository->friendIds();
        $idCurrent = $this->userRepository->user();
        $ids =[];
        for ($i=0; $i < count($listFriends); $i++) {
            array_push($ids, $listFriends[$i]->user_id, $listFriends[$i]->friend_id);
        }
        array_unique($ids);
        $newIds = array_unique($ids);
        for ($i=0; $i < count($newIds); $i++) {
            if ($newIds[$i]== $idCurrent) {
                unset($newIds[$i]);
            }
        }

        return $this->userRepository->listUserByIds($newIds);
    }
}
