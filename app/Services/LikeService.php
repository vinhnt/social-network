<?php

namespace App\Services;

use App\Services\LikeServiceInterface;
use App\Repositories\LikeRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LikeService implements LikeServiceInterface
{
    private $likeRepository;
    public function __construct(LikeRepository $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }
///
    public function getAll()
    {
        $likes = $this->likeRepository->getAll();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get all like',
            'likes' => $likes
        ]);
    }
///
    public function getByUserId($id)
    {

        $likes = $this->likeRepository->getByUserId($id);
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get like by User ID',
            'likes' => $likes
        ]);
    }
///
    public function createLike($request)
    {

        $validator = Validator::make($request->all(), [
            'likeable_id' => 'required|numeric',
            'likeable_type' => 'required|string|max:255',
        ], [
            'likeable_id.required'=>'Khong duoc de trong',
            'likeable_id.numeric'=>'Phai la dang ky tu so',
            'likeable_typerequired'=>'Khong duoc de trong',
            'likeable_type.string'=>'Phai la dang ky tu chuoi',
            'likeable_type.max'=>'Toi da 255 ky tu',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validate error',
                'validate'=>$validator->customMessages,
            ]);
        }

        try {
            $like = $this->likeRepository->createLike([
                'likeable_id' => $request->likeable_id,
                'likeable_type' => $request->likeable_type,
                'user_id' => Auth::guard('api')->user()->id,
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'Successfully create like by User ID',
                'like' => $like
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => "Sorry, can't create like by user id now",
            ]);
        }
    }

///
    public function unLike($request)
    {

        $validator = Validator::make($request->route()->parameters(), [
            'id' => 'required|numeric',
        ], [
            'id.required'=>'khong duoc de trong',
            'id.numeric'=> 'id phai la so'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validate error',
                'validate'=>$validator->customMessages
            ]);
        }
        // dd($request->route('id'));
        $like = $this->likeRepository->unLike($request->route('id'));
        if (!$like) {
            return response()->json([
                'status' => 'error',
                'message' => "Error, don't  unlike by User ID",
            ], 401);
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully  unlike by User ID',
        ]);
    }
///
    public function getByPostId($request)
    {

        $validator = Validator::make($request->route()->parameters(), [
            'id' => 'required|numeric',
        ], [
            'id.required'=>'khong duoc de trong',
            'id.numeric'=> 'id phai la so'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validate error',
                'validate'=>$validator->customMessages
            ]);
        }

        $likes = $this->likeRepository->getByPostId($request->route('id'));
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get like by Post ID',
            'likes' => $likes
        ]);
    }
}
