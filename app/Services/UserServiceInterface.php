<?php

namespace App\Services;

use App\Services\GeneralService;

interface UserServiceInterface extends GeneralService
{
    public function getAll();
    public function updateUserProfile($request);
    public function changePassword($data);
    public function creatUser($users);
    public function forgetPassword($data);
    public function updatePassword($data);
    public function infor();
    public function getUserById($data);
    public function delete($id);
    public function edit($id);
}
