<?php

namespace App\Services\Admin;

use App\Services\GeneralService;

interface AdminServiceInterface extends GeneralService
{
    public function getAll($id);
    public function getProfileById();
    public function updateProfile($request);
    public function updatePassword($request);
}
