<?php

namespace App\Services\Admin;

use App\Services\Admin\AdminServiceInterface;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AdminService implements AdminServiceInterface
{
    private $admintRepository;

    public function __construct(AdminRepository $admintRepository)
    {
        $this->admintRepository = $admintRepository;
    }
///
    public function getAll($id)
    {
        return $this->admintRepository->getAll($id);
    }

    public function getProfileById()
    {
        $id = auth('web')->user()->id;
        $editData = $this->admintRepository->find($id);
        return view('admin.pages.edit_profile', compact('editData'));
    }
///
    public function updateProfile($request)
    {
        $validated = $request->validate([
            'username' => 'required|max:100',
            'image_avatar' => 'nullable|image|mimes:jpg,png,jpeg',
        ], [
            'username.required'=>'Username không được để trống',
            'username.max'=>'Username tối đa 100 kí tự',
            'image_avatar.image' => 'Không đúng định dạng image',
            'image_avatar.mimes' => 'Chỉ hỗ trợ phần mở rộng:jpg,png,jpeg',
        ]);

        $data = array('username' => $request->username);
        if ($request->hasfile('image_avatar')) {
            $file = $request->file('image_avatar');
            $fileName = time().'_'.$file->getClientOriginalName();
            $file->move(public_path('uploads/admins/avatar'), $fileName);
            $data['image_avatar'] = $fileName;
            if (!empty(auth('web')->user()->image_avatar)) {
                unlink(public_path('uploads/admins/avatar/'.auth('web')->user()->image_avatar));
            }
        }

        $id = auth('web')->user()->id;
        try {
            $this->admintRepository->update($id, $data);
            $notification = array(
                'message' => 'Updated profile successfully',
                'alert-type' => 'success'
            );
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Sorry! can\'t not update profile now',
                'alert-type' => 'error'
            );
        }
        return redirect()->route('edit.profile')->with($notification);
    }
///
    public function updatePassword($request)
    {

        $validated = $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        ], [
            'old_password.required'=>'Không được để trống',
            'new_password.required'=>'Không được để trống',
            'new_password.min'=>'Tối thiểu 8 ký tự',
            'confirm_password.required'=>'Không được để trống',
            'confirm_password.same'=>'Mật khẩu mới nhập lại không trùng nhau',
        ]);

        if (!Hash::check($request->old_password, auth('web')->user()->password)) {
            $notification = array(
                'message' => 'Sorry! old password not match ',
                'alert-type' => 'error'
            );
            return redirect()->route('change.password')->with($notification);
        }

        try {//done thi logout
            $id = auth('web')->user()->id;
            $user = $this->admintRepository->update($id, ['password'=> Hash::make($request->new_password)]);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Sorry! can\'t not update password now',
                'alert-type' => 'error'
            );
            return redirect()->route('change.password')->with($notification);
        }
        return redirect()->route('logout');
    }
///
}
