<?php

namespace App\Services;

use App\Services\GeneralService;

interface FriendServiceInterface extends GeneralService
{
    public function delete($id);
    public function list();
}
