<?php

namespace App\Repositories\Interfaces;

interface LikeRepositoryInterface
{
    public function find($id);
    public function getAll();
    public function getByUserId($id);
    public function getByPostId($id);
    public function createLike($like);
    public function unLike($like);
}
