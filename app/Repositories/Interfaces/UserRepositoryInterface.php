<?php

namespace App\Repositories\Interfaces;

interface UserRepositoryInterface
{
    public function find($id);
    public function getAll();
    public function creatUser($user);
    public function checkEmail($email);
    public function updateUserProfileWithImage($filename, $name);
    public function updateUserProfileWithoutImage($name);
    public function changePassword($data);
    public function user();
    public function updateUuid($data, $email);
    public function checkUuid($uuid);
    public function updatePassword($data);
    public function listUserByIds($ids);
    public function getUser();
    public function getUserById($id);
    public function delete($id);
    public function edit($id);
}
