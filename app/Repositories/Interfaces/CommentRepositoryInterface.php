<?php

namespace App\Repositories\Interfaces;

interface CommentRepositoryInterface
{
    public function getByPostId($id);
    public function deleteComment($id);
    public function editComment($comment);
    public function create($data);
}
