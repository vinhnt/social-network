<?php

namespace App\Repositories\Interfaces;

interface PostRepositoryInterface
{
    public function create($data);
    public function delete($id);
    public function postOfUser($id);
    public function update($data, $id);
    public function list($userIds);
    public function all();
}
