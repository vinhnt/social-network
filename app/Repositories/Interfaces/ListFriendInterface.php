<?php

namespace App\Repositories\Interfaces;

interface ListFriendInterface
{
    public function listFriend($id);
    public function delete($id);
    public function isFriend($id);
    public function friendIds();
}
