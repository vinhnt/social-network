<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ListFriendInterface;
use App\Models\ListFriend;
use Illuminate\Support\Facades\Auth;

class ListFriendRepository implements ListFriendInterface
{
    public function listFriend($id)
    {
        return ListFriend::where([
                ['user_id', '=', $id],
                ['status', '=', 1]
            ])->get();
    }

    public function delete($id)
    {
        ListFriend::where(function ($query) use ($id) {
            $query->where('friend_id', '=', $id)
            ->where('user_id', '=', auth('api')->user()->id);
        })->orWhere(function ($query) use ($id) {
            $query->where('user_id', '=', $id)
            ->where('friend_id', '=', auth('api')->user()->id);
        })->delete();
        return [
            'message'=> 'Delete your friend is successfully'
        ];
    }

    public function isFriend($id)
    {
        if (
            count(ListFriend::where(function ($query) use ($id) {
                $query->where('friend_id', '=', $id)
                ->where('user_id', '=', auth('api')->user()->id)
                ->where('status', '=', 0);
            })->get()) > 0
        ) {
            return true;
        }
        return false;
    }

    public function friendIds()
    {
        // return ListFriend::where([
        //     ['user_id', '=', auth('api')->user()->id],
        //     ['status', '=', 1]
        // ])->pluck('friend_id')->toArray();
        return ListFriend::where(function ($query) {
            $query->where('user_id', '=', auth('api')->user()->id)
            ->where('status', '=', 1);
        })->orWhere(function ($query) {
            $query->where('friend_id', auth('api')->user()->id)
            ->where('status', '=', 1);
        })->get();
    }
}
