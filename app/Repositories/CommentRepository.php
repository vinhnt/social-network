<?php

namespace App\Repositories;

use App\Models\Comments;
use App\Repositories\Interfaces\CommentRepositoryInterface;

class CommentRepository implements CommentRepositoryInterface
{
    public function getByPostId($id)
    {
        return Comments::where('post_id', $id)->get();
    }

    public function editComment($comment)
    {
        return Comments::where('id', $comment->id)->update(['content'=>$comment->content]);
    }

    public function deleteComment($id)
    {
        return Comments::where('id', $id)->delete();
    }
    public function create($data)
    {
        return Comments::create($data);
    }
}
