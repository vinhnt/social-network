<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{
    public function find($id)
    {
        return User::find($id);
    }

    public function getAll()
    {
        return User::paginate(8);
    }

    public function updateUserProfileWithImage($filename, $name)
    {
        User::find(auth('api')->user()->id)->update([
            'name' => $name,
            'image_avatar' => $filename
        ]);
        DB::table('photos')->insert([
            'user_id'=> auth('api')->user()->id,
            'photo'=> $filename
        ]);
        return [
            'message' => 'Update Profile User With Image Successfully'
        ];
    }

    public function updateUserProfileWithoutImage($name)
    {
        User::find(auth('api')->user()->id)->update([
            'name' => $name
        ]);
        return [
            'message' => 'Update Profile User Without Image Successfully'
        ];
    }

    public function changePassword($data)
    {
        $user = auth('api')::user();
        if (Hash::check($data['old_password'], $user->password)) {
            $user->password = Hash::make($data['new_password']);
            $user->save();
            return [
                'message' => 'Change password is successfully'
            ];
        }
        return [
            'message' => 'Old password is not correct. Please try it again!',
            'status' => 422
        ];
    }

    public function creatUser($user)
    {
        return User::create($user);
    }

    public function checkEmail($email)
    {
        return User::where('email', $email)->firstOrFail();
    }
    public function user()
    {
        return auth('api')->user()->id;
    }

    public function updateUuid($data, $email)
    {
        User::where('email', $email)->update([
            'uuid'=> $data
        ]);
    }

    public function checkUuid($uuid)
    {
        return User::where('uuid', $uuid)->first();
    }

    public function updatePassword($data)
    {
        User::where('uuid', $data['uuid'])->update([
            'password'=> Hash::make($data['password'])
        ]);
        return [
            'message' => 'Change Password is successfully'
        ];
    }

    public function listUserByIds($ids)
    {
        return User::whereIn('id', $ids)->get();
    }

    public function getUser()
    {
        return auth('api')->user();
    }

    public function infor()
    {
        return auth('api')->user();
    }

    public function getUserById($id)
    {
        return User::where('id', $id)->get();
    }

    public function delete($id)
    {
        User::findorFail($id)->delete($id);
    }

    public function edit($id)
    {
        return User::find($id);
    }
}
