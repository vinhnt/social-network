<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\Interfaces\PostRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class PostRepository implements PostRepositoryInterface
{
    public function create($data)
    {
        $post = new Post();
        $post->content = $data['content'];
        $post->user_id = auth('api')->user()->id;
        $post->status = $data['status'];
        $post->save();
        return [
            'message' => 'Add Post is successfully'
        ];
    }

    public function delete($id)
    {
        Post::find($id)->delete();
        return [
                'message' => 'This post deleted successfully'
            ];
    }

    public function find($id)
    {
        return Post::find($id);
    }

    public function postOfUser($id)
    {
        if (auth('api')::user()->id != Post::find($id)->user_id) {
            return false;
        }
        return true;
    }

    public function update($data, $id)
    {
        Post::find($id)->update([
            'content' => $data['content'],
            'status' => $data['status']
        ]);
        return [
            'message' => 'Update post is successfully'
        ];
    }

    public function list($userIds)
    {
        return Post::where(function ($query) use ($userIds) {
            $query->whereIn('user_id', $userIds)
            ->where('status', '=', 1);
        })->orWhere(function ($query) {
            $query->where('user_id', auth('api')->user()->id)
            ->where('status', '=', 1);
        })->orderBy('created_at', 'DESC')->with('user', 'comments.user')->get();
    }
    public function all()
    {
        return Post::all()->load('user');
    }
}
