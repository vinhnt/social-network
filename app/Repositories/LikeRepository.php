<?php

namespace App\Repositories;

use App\Models\Like;
use App\Repositories\Interfaces\LikeRepositoryInterface;

class LikeRepository implements LikeRepositoryInterface
{
    public function find($id)
    {
        return Like::find($id);
    }

    public function getAll()
    {
        return Like::all();
    }

    public function getByUserId($id)
    {
        return Like::where('user_id', $id)->get();
    }

    public function getByPostId($id)
    {
        return Like::where('likeable_type', 'App/Models/Post')->where('likeable_id', $id)->get();
    }

    public function createLike($like)
    {
        return Like::create($like);
    }

    public function unLike($like)
    {
        return Like::destroy($like);
    }
}
