<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PhotoRepositoryInterface;
use App\Models\Photo;
use Illuminate\Support\Facades\Auth;

class PhotoRepository implements PhotoRepositoryInterface
{
    public function index()
    {
        return Photo::where('user_id', '=', auth('api')->user()->id)->get();
    }
}
