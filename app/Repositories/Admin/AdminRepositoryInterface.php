<?php

namespace App\Repositories\Admin;

interface AdminRepositoryInterface
{
    public function getall($id);
    public function find($id);
    public function update($id, $data);
}
