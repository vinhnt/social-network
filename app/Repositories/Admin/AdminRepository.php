<?php

namespace App\Repositories\Admin;

use App\Models\Admins;
use App\Repositories\Admin\AdminRepositoryInterface;

class AdminRepository implements AdminRepositoryInterface
{
    public function getall($id)
    {
        if ($id) {
            $abc = (int)$id;
            return Admins::where('is_admin', (int)$id)->get();
        } else {
            return Admins::paginate(8);
        }
    }

    public function find($id)
    {
        return Admins::find($id);
    }

    public function update($id, $data)
    {
        return Admins::where('id', $id)->update($data);
    }
}
