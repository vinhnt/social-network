<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailForgetPassword extends Mailable
{
    use Queueable;
    use SerializesModels;

    // public $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $code;
    public $link;
    public function __construct($code, $link)
    {
        $this->code =$code;
        $this->link =$link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Mail From Social NetWork')
                    ->view('emails.forgetPassWord');
    }
}
