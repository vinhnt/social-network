<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Services\UserService;

class UserController extends Controller
{
    public $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function updateInfor(Request $request)
    {
        return response()->json($this->userService->updateUserProfile($request));

        //Validation name, image_avatar
        // $validator = Validator::make($request->all(), [
        //     'name' => 'required',
        //     'image_avatar' => 'nullable|image|mimes:jpg,png,jpeg'
        // ]);
        // if ($validator->fails()) {
        //     return response()->json([
        //         'message' => 'Validator fails',
        //         'error' => $validator->errors()
        //     ], 422);
        // }
        // //Check file exists
        // if ($request->hasfile('image_avatar')) {
        //     $file=$request->file('image_avatar');
        //     $filename=date('YmdHi').$file->getClientOriginalName();
        //     $file->move(public_path('uploads/user'), $filename);
        //     User::FindOrFail(1)->update([
        //         'name' => $request->name,
        //         'image_avatar' => $filename
        //     ]);
        //     return response()->json([
        //         'message' => 'Update infor user successfully with image avatar'
        //     ]);
        // }
        // // Update without file image
        // User::FindOrFail(1)->update([
        //     'name' => $request->name
        // ]);
        // return response()->json([
        //     'message' => 'Update infor user successfully without image avatar'
        // ]);
    }

    public function changePassword(Request $request)
    {
        // Validation oldPassword, newPassword, confirmPassword
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validator fails',
                'error' => $validator->errors()
            ], 422);
        }
        $user = Auth::user();
        //Bam password check input
        if (Hash::check($request->old_password, $user->password)) {
            $user->password = Hash::make($request->new_password);
            $user->save();
            return response()->json([
                'message' => 'Change password is successfully'
            ]);
        }
        return response()->json([
            'message' => 'Old password is not correct. Please try it again!'
        ], 422);
    }

    public function getAll()
    {
        return response()->json($this->userService->getAll());
    }
}
