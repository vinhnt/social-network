<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Admin\AdminService;

class AdminController extends Controller
{
    private $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    public function allAdmin($id)
    {
        $data = $this->adminService->getAll($id);
        return view('admin.pages.list_admin', compact('data'));
    }

    public function getProfileById()
    {
        return $this->adminService->getProfileById();
    }

    public function updateProfile(Request $request)
    {
        return $this->adminService->updateProfile($request);
    }

    public function viewUpdatePassword()
    {
        return view('admin.pages.change_password');
    }

    public function updatePassword(Request $request)
    {
        return $this->adminService->updatePassword($request);
    }
}
