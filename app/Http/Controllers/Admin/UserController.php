<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function allUser()
    {
        $data = $this->userService->getAll();
        return view('admin.pages.list_user', compact('data'));
    }

    public function deleteUser($id)
    {
        $this->userService->delete($id);

        $notification = array(
            'message' => 'Delete User is Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('all.user')->with($notification);
    }

    public function editUser($id)
    {
        $data = $this->userService->edit($id);
        return view('admin.pages.edit_user', compact('data'));
    }
}
