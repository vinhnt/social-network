<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;

class AuthController extends Controller
{
//contruct
    private $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
//login
    public function login(Request $request)
    {
        return $this->userService->login($request);
    }
//register
    public function register(Request $request)
    {
        return $this->userService->register($request);
    }
//logout
    public function logout()
    {
        return $this->userService->logout();
    }
//refresh
    public function refresh()
    {
        return $this->userService->refresh();
    }
//
    public function updateProfile(Request $request)
    {
        return response()->json($this->userService->updateUserProfile($request));
    }

    public function changePassword(Request $request)
    {
        $data = $request->all();
        return response()->json($this->userService->changePassword($data));
    }

    public function forgetPassword(Request $request)
    {
        $data = $request->all();
        return response()->json($this->userService->forgetPassword($data));
    }

    public function updatePassword(Request $request)
    {
        $data = $request->all();
        return response()->json($this->userService->updatePassword($data));
    }

    public function infor()
    {
        $data = $this->userService->infor();
        return view('users.index.user_current', compact('data'))->render();
    }

    public function getUserById(Request $request)
    {
        $data = $request->all();
        return response()->json($this->userService->getUserById($data));
    }

    public function userCurrent()
    {
        $data = $this->userService->infor();
        return view('users.user_infor', compact('data'))->render();
    }

    public function userSetting()
    {
        $data = $this->userService->infor();
        return view('users.user_setting', compact('data'))->render();
    }

    public function about()
    {
        $data = $this->userService->infor();
        return view('users.about_infor', compact('data'))->render();
    }
}
