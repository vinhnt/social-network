<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PhotoService;

class PhotoController extends Controller
{
    public $photoService;
    public function __construct(PhotoService $photoService)
    {
        $this->photoService = $photoService;
    }
    public function index()
    {
        $data = $this->photoService->index();
        $count = count($data);
        return view('users.listphoto', compact(['data', 'count']))->render();
    }
}
