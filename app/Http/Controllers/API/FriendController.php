<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\FriendService;

class FriendController extends Controller
{
    public $friendService;

    public function __construct(FriendService $friendService)
    {
        $this->friendService = $friendService;
    }

    public function delete($id)
    {
        return response()->json($this->friendService->delete($id));
    }

    public function list()
    {
        $data = $this->friendService->list();
        $count = count($data);
        return view('users.listfriend', compact(['data','count']))->render();
    }
}
