<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\LikeService;
use Illuminate\Support\Facades\Auth;

class Like extends Controller
{
    private $likeService;
    public function __construct(LikeService $likeService)
    {
        $this->likeService = $likeService;
    }

    public function getAll()
    {
        return $this->likeService->getAll();
    }

    public function getByUserId()
    {
        return $this->likeService->getByUserId(Auth::guard('api')->user()->id);
    }

    public function getByPostId(Request $request)
    {
        return $this->likeService->getByPostId($request);
    }

    public function createLike(Request $request)
    {

        return $this->likeService->createLike($request);
    }

    public function unLike(Request $request)
    {
        return $this->likeService->unLike($request);
    }
}
