<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PostService;
use Illuminate\Support\Facades\Hash;

class PostController extends Controller
{
    public $postService;
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function create(Request $request)
    {
        $data = $request->all();
        return response()->json($this->postService->create($data));
    }

    public function delete($id)
    {
        return response()->json($this->postService->delete($id));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        return response()->json($this->postService->update($data, $id));
    }

    public function list()
    {
        $data = $this->postService->list();
        $user = $this->postService->getUser();
        return view('list', compact(['data', 'user']))->render();
    }
    public function allPost()
    {
        return $this->postService->allPost();
    }
}
