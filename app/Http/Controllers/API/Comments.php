<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\CommentService;

class Comments extends Controller
{
    private $commentService;
    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }
///
    public function getByPostId(Request $request)
    {
        return $this->commentService->getByPostId($request);
    }
///
    public function editComment(Request $request)
    {
        return $this->commentService->editComment($request);
    }
///
    public function deleteComment(Request $request)
    {
        return $this->commentService->deleteComment($request);
    }
///
    public function createComment(Request $request)
    {
        return $this->commentService->createComment($request);
    }
}
