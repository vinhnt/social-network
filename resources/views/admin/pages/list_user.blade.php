
@extends('admin.master_index.master')

@section('contend')
<div class="page-content">
                    <div class="container-fluid">   
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                    <h4 class="mb-sm-0">DASH BOARD</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Upcube</a></li>
                                            <li class="breadcrumb-item active">Dashboard</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <p class="text-truncate font-size-14 mb-2">Total User</p>
                                                <h4 class="mb-2">{{ count($data) }}</h4>
                                            </div>
                                            <div class="avatar-sm">
                                                <span class="avatar-title bg-light text-primary rounded-3">
                                                    <i class="ri-shopping-cart-2-line font-size-24"></i>  
                                                </span>
                                            </div>
                                        </div>                                            
                                    </div><!-- end cardbody -->
                                </div><!-- end card -->
                            </div><!-- end col -->
                        </div><!-- end row -->
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4">LIST USER </h4>
    
                                        <div class="table-responsive">
                                            <table class="table table-centered mb-0 align-middle table-hover table-nowrap">
                                                <thead class="table-light">
                                                    <tr>
                                                        <th>STT</th>
                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead><!-- end thead -->
                                                <tbody>
                                                    <?php $t=0; ?>
                                                    @foreach ($data as $item)
                                                        <tr>
                                                            <td>{{ ++$t }}</td>
                                                            <td>{{ $item->id }}</td>
                                                            <td><h6 class="mb-0">{{ $item->name }}</h6></td>
                                                            <td>{{ $item->email }}</td>
                                                            <td>
                                                                <img src="{{ asset('users/images/resources/'.$item->image_avatar) }}" alt="" style="width:50px">
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('user.edit', $item->id) }}" class="btn btn-info sm" title="Edit User"><i class="fas fa-edit"></i></a>
                                                                <a href="{{ route('user.delete', $item->id) }}" class="btn btn-danger sm" title="Delete User" id="delete"><i class="fas fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    
                                                </tbody><!-- end tbody -->
                                            </table>
                                            <!-- end table -->
                                        </div>
                                        
                                        
                                    </div><!-- end card -->
                                </div><!-- end card -->
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    
                </div>
@endsection
