@extends('admin.master_index.master')

@section('contend')
{{-- <div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit User</h4>
                        <!-- end row -->
                        <form action="{{ route('blog.update',$blog->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Blog Category Name</label>
                                <div class="col-sm-10">
                                    <select class="form-select" aria-label="Default select example" name="blog_category_id">
                                        <option>Open this select menu</option>
                                        @foreach ($blogCategory as $item)
                                            <option value="{{ $item->id }}" @if ($item->id==$blog->blog_category_id) selected="selected"
                                                
                                            @endif>{{ $item->blog_category}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Blog Title</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="example-search-input" name="blog_title" value="{{ $blog->blog_title }}">
                                    @error('blog_title')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Blog Tag</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" data-role="tagsinput" name="blog_tags" value="{{ $blog->blog_tags }}">
                                </div>
                            </div>
                    
                            <div class="row mb-3">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Blog Description</label>
                                <div class="col-sm-10">
                                <textarea id="elm1" name="blog_description">{{ $blog->blog_description }}</textarea>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Blog Image</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="file" id="image" name="blog_image">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="example-search-input" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <img class="rounded avatar-lg" src="{{ asset($blog->blog_image) }}" alt="Card image cap" id="showImage">
                                </div>
                            </div>
                            <input type="submit" value="Update Blog  Data" class="btn btn-info waves-effect waves-light"/>
                        </form>
                        
                        <!-- end row -->
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#image').change(function(e) {
            var reader= new FileReader();
            reader.onload=function(e) {
                $('#showImage').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
    });
</script> --}}
                   
@endsection
