@extends('admin.auth.master')

@section('form')
 <div class="card-body">

<div class="text-center mt-4">
    <div class="mb-3">
        <a href="index.html" class="auth-logo">
            <img src="admin/images/logo2.png" >
        </a>
    </div>
</div>

<h4 class="text-muted text-center font-size-18"><b>Sign In</b></h4>

<div class="p-3">
    <form class="form-horizontal mt-3" method="POST" action="{{ route('login') }}">
    <x-auth-session-status class="mb-4 text-success" :status="session('status')" />
    <x-auth-validation-errors class="mb-4 text-danger" :errors="$errors" />
    @csrf
        <div class="form-group mb-3 row">
            <div class="col-12">
                <input class="form-control" type="email" name="email" value="{{old('email')}}" required placeholder="Email" autofocus>
            </div>
        </div>

        <div class="form-group mb-3 row">
            <div class="col-12">
                <input class="form-control" type="password" name="password" required placeholder="Password">
            </div>
        </div>

        <div class="form-group mb-3 row">
            <div class="col-12">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="remember" id="customCheck1">
                    <label class="form-label ms-1" for="customCheck1">Remember me</label>
                </div>
            </div>
        </div>

        <div class="form-group mb-3 text-center row mt-3 pt-1">
            <div class="col-12">
                <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Log In</button>
            </div>
        </div>

        <div class="form-group mb-0 row mt-2">
            <div class="col-sm-7 mt-3">
                <a href="{{ route('password.request') }}" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
            </div>
            <div class="col-sm-5 mt-3">
                <a href="{{ route('admin.register') }}" class="text-muted"><i class="mdi mdi-account-circle"></i> Create an account</a>
            </div>
        </div>
    </form>
</div>
<!-- end -->
</div>
@endsection