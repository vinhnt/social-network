@extends('admin.auth.master')

@section('form')
<div class="card-body">
    
    <div class="text-center mt-4">
        <div class="mb-3">
            <a href="index.html" class="auth-logo">
            <img src="admin/images/logo2.png" >
            </a>
        </div>
    </div>

    <h4 class="text-muted text-center font-size-18"><b>Reset Password</b></h4>

    <div class="p-3">
        <form class="form-horizontal mt-3" method="POST" action="{{ route('password.email') }}">
        <x-auth-session-status class="mb-4 text-info" :status="session('status')" />
        <x-auth-validation-errors class="mb-4 text-danger" :errors="$errors" />
        @csrf
            <p>Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one. !</p>
            <div class="form-group mb-3">
                <div class="col-xs-12">
                    <input class="form-control" type="email" required="" name='email' value="{{old('email')}}" placeholder="Email">
                </div>
            </div>

            <div class="form-group pb-2 text-center row mt-3">
                <div class="col-12">
                    <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Send Email</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection