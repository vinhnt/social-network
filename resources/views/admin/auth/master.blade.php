<!doctype html>
<html lang="en">

    <head>
    
        <meta charset="utf-8" />
        <title>Login | Upcube - Admin & Dashboard Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesdesign" name="author" />
        <base href="{{asset('')}}"/>
        <!-- App favicon -->
        <link rel="shortcut icon" href="admin/images/favicon.ico">

        <!-- Bootstrap Css -->
        <link href="admin/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="admin/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="admin/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
         <!-- Toast -->
        <link rel="stylesheet" type="text/css" href="admin/libs/toastr/build/toastr.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.css" >

    </head>

    <body class="auth-body-bg">
        <div class="bg-overlay"></div>
        <div class="wrapper-page">
            <div class="container-fluid p-0">
                <div class="card">
                    @yield('form') 
                    <!-- end cardbody -->
                </div>
                <!-- end card -->
            </div>
            <!-- end container -->
        </div>
        <!-- end -->

        <!-- JAVASCRIPT -->
        <script src="admin/libs/jquery/jquery.min.js"></script>
        <script src="admin/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="admin/libs/metismenu/metisMenu.min.js"></script>
        <script src="admin/libs/simplebar/simplebar.min.js"></script>
        <script src="admin/libs/node-waves/waves.min.js"></script>

        <!-- toastr plugin -->
        <script src="admin/libs/toastr/build/toastr.min.js"></script>
        <!-- toastr init -->
        <script src="admin/js/pages/toastr.init.js"></script>

        <script>
            @if(Session::has('message'))
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            var type = "{{ Session::get('alert-type') }}"
            switch(type){
                case 'info':
                    toastr["info"](" {{ Session::get('message') }} ");
                break;
                case 'success':
                    toastr["success"](" {{ Session::get('message') }} ");
                break;
                case 'warning':
                    toastr["warning"](" {{ Session::get('message') }} ");
                break;
                case 'error':
                    toastr["error"](" {{ Session::get('message') }} ");
                break; 
            }
            @endif 
        </script>

        <script src="admin/js/app.js"></script>
    </body>
</html>
