@extends('admin.auth.master')

@section('form')
 <div class="card-body">

<div class="text-center mt-4">
    <div class="mb-3">
        <a href="index.html" class="auth-logo">
            <img src="admin/images/logo2.png" >
        </a>
    </div>
</div>

<div class="p-3">
    <form class="form-horizontal mt-3" method="POST" action="{{ route('password.update') }}">
    <x-auth-session-status class="mb-4" :status="session('status')" />
    <x-auth-validation-errors class="mb-4 text-danger" :errors="$errors" />
    @csrf
        <input type="hidden" name="token" value="{{ $request->route('token') }}">
        <div class="form-group mb-3 row">
            <div class="col-12">
                <input class="form-control" type="email" name="email" value="{{old('email', $request->email)}}" required placeholder="Email" autofocus>
            </div>
        </div>

        <div class="form-group mb-3 row">
            <div class="col-12">
                <input class="form-control" type="password" name="password" required placeholder="Password">
            </div>
        </div>

        <div class="form-group mb-3 row">
            <div class="col-12">
                <input class="form-control" type="password" name="password_confirmation" required placeholder="Confirm Password">
            </div>
        </div>

        <div class="form-group mb-3 text-center row mt-3 pt-1">
            <div class="col-12">
                <button class="btn btn-info w-100 waves-effect waves-light" type="submit">Reset Password</button>
            </div>
        </div>

       
    </form>
</div>
<!-- end -->
</div>
@endsection