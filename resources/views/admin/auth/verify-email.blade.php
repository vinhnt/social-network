@extends('admin.auth.master')

@section('form')
<div class="card-body">
    
    <div class="text-center mt-4">
        <div class="mb-3">
            <a href="index.html" class="auth-logo">
                <img src="admin/images/logo2.png" >
            </a>
        </div>
    </div>

    <h4 class="text-muted text-center font-size-18"><b></b></h4>
    
    <div class="mb-4 text-sm text-gray-600">
        {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
    </div>

    @if (session('status') == 'verification-link-sent')
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ __('A new verification link has been sent to the email address you provided during registration.') }}
        </div>
     @endif

    <div class="mt-4 flex">
        <div class="row">
            <div class="col-md-9">
                <form method="POST" action="{{ route('verification.send') }}">
                    @csrf
                    <div>
                    <button type="button" class="btn btn-outline-primary">Resend Verification Email</button>
                    </div>
                </form>
            </div>
            <div class="col-md-3 right">
                <a class="btn btn-outline-dark" href="{{ route('logout') }}"> Logout </a>
            </div>
        </div>
    </div>
</div>
@endsection
