<div class="vertical-menu">

<div data-simplebar class="h-100">
    <!--- Sidemenu -->
    <div id="sidebar-menu">
        <!-- Left Menu Start -->
        <ul class="metismenu list-unstyled" id="side-menu">
            <li class="menu-title">Menu</li>

            <li>
                <a href="index.html" class="waves-effect">
                    <i class="ri-dashboard-line"></i><span class="badge rounded-pill bg-success float-end">3</span>
                    <span>Dashboard</span>
                </a>
            </li>

            <li>
                <a href="{{ route('all.user') }}" class=" waves-effect">
                    <i class="ri-calendar-2-line"></i>
                    <span>List User</span>
                </a>
            </li>

            <li>
                <a href="calendar.html" class=" waves-effect">
                    <i class="ri-calendar-2-line"></i>
                    <span>List Post</span>
                </a>
                
            </li>

            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="ri-layout-3-line"></i>
                    <span>Admin</span>
                </a>
                <ul class="sub-menu" aria-expanded="true">
                    <li>
                        <a href="/admin/1" >Reviewer</a>
                    </li>
                    <li>
                        <a href="/admin/2">Editor</a>
                    </li>
                    <li>
                        <a href="/admin/3">Manager</a>
                    </li>
                    <li>
                        <a href="/admin/4">Master</a>
                    </li>    
                </ul>
            </li>
        </ul>
    </div>
    <!-- Sidebar -->
</div>
</div>
