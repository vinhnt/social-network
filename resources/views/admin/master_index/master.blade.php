<!doctype html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title>Dashboard | Upcube - Admin & Dashboard Template</title>
        <base href="{{ asset('')}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesdesign" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="admin/images/favicon.ico">

        <!-- jquery.vectormap css -->
        <!--  <link href="admin/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" /> -->

        <!-- DataTables -->
        <link href="admin/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Responsive datatable examples -->
        <link href="admin/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />  

        <!-- Bootstrap Css -->
        <link href="admin/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="admin/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="admin/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
        <!-- Toast -->
        <link rel="stylesheet" type="text/css" href="admin/libs/toastr/build/toastr.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.css" >
        <!-- Sweet Alert-->
        <link href="admin/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <script src="admin/libs/jquery/jquery.min.js"></script>

        @yield('link')
    </head>

    <body data-topbar="dark">
    
    <!-- <body data-layout="horizontal" data-topbar="dark"> -->

        <!-- Begin page -->
        <div id="layout-wrapper">
           
            @include('admin.master_index.header')
            <!-- ========== Left Sidebar Start ========== -->
            @include('admin.master_index.sidebar')
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

             
            @yield('contend')
                <!-- End Page-content -->
               
            @include('admin.master_index.footer')
                
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- JAVASCRIPT -->
        <script src="admin/libs/jquery/jquery.min.js"></script>
        <script src="admin/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="admin/libs/metismenu/metisMenu.min.js"></script>
        <script src="admin/libs/simplebar/simplebar.min.js"></script>
        <script src="admin/libs/node-waves/waves.min.js"></script>

        
        <!-- apexcharts -->
        <!-- <script src="admin/libs/apexcharts/apexcharts.min.js"></script> -->

        <!-- jquery.vectormap map -->
         <!-- <script src="admin/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js"></script> -->
         <!-- <script src="admin/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-us-merc-en.js"></script> -->

        <!-- Required datatable js -->
        <script src="admin/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="admin/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        
        <!-- Responsive examples -->
        <script src="admin/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="admin/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

        <script src="admin/js/pages/dashboard.init.js"></script>

        <!-- toastr plugin -->
        <script src="admin/libs/toastr/build/toastr.min.js"></script>

        <!-- toastr init -->
        <script src="admin/js/pages/toastr.init.js"></script>
        <script>
            @if(Session::has('message'))
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            var type = "{{ Session::get('alert-type') }}"
            switch(type){
                case 'info':
                    toastr["info"](" {{ Session::get('message') }} ");
                break;
                case 'success':
                    toastr["success"](" {{ Session::get('message') }} ");
                break;
                case 'warning':
                    toastr["warning"](" {{ Session::get('message') }} ");
                break;
                case 'error':
                    toastr["error"](" {{ Session::get('message') }} ");
                break; 
            }
            @endif 
        </script>
        <!-- DataTable js -->
        <script src="admin/js/pages/datatables.init.js"></script>
        <script src="admin/libs/sweetalert2/sweetalert2.min.js"></script>
        <!-- Sweet alert init js-->
        <script src="admin/js/pages/sweet-alerts.init.js"></script>
        <!-- Sweet alert delete btn-->
        <script src="admin/js/delete.js"></script>
        
        <!-- App js -->
        <script src="admin/js/app.js"></script>
        <script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.min.js" ></script>
        <!-- show img input upload file -->
        <script type="text/javascript">
            $(document).ready(function(){
                $('#image').change(function(e){
                    var reader = new FileReader();
                    reader.onload = function(e){
                        $('#showImage').attr('src',e.target.result);
                    }
                    reader.readAsDataURL(e.target.files['0']);
                });
            });
        </script>
        @yield('script')
    </body>

</html>
