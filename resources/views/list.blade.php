@foreach ($data as $item)
<div class="friend-info">
    <figure>
        <img src="{{ asset('users/images/resources/'.$item->user->image_avatar) }}" alt="">
    </figure>
    <div class="friend-name">
        <div class="more">
            <div class="more-post-optns"><i class="ti-more-alt"></i>
                <ul>
                    <li><i class="fa fa-pencil-square-o"></i>Edit Post</li>
                    <li><i class="fa fa-trash-o"></i>Delete Post</li>
                    <li class="bad-report"><i class="fa fa-flag"></i>Report Post</li>
                    <li><i class="fa fa-address-card-o"></i>Boost This Post</li>
                    <li><i class="fa fa-clock-o"></i>Schedule Post</li>
                    <li><i class="fa fa-wpexplorer"></i>Select as featured</li>
                    <li><i class="fa fa-bell-slash-o"></i>Turn off Notifications</li>
                </ul>
            </div>
        </div>
        <ins><a href="time-line.html" title="">{{ $item->user->name }}</a> Post Album</ins>
    </div>
    <div class="post-meta">
        <p>
            {{ $item->content }}
        </p>	
        <div class="we-video-info">
            <ul>
                <li>
                    <span class="views" title="views">
                        <i class="fa fa-eye"></i>
                        <ins>1.2k</ins>
                    </span>
                </li>
                <li>
                    <div class="likes heart" title="Like/Dislike">❤ <span>2K</span></div>
                </li>
                <li>
                    <span class="comment" title="Comments">
                        <i class="fa fa-commenting"></i>
                        <ins>52</ins>
                    </span>
                </li>

                <li>
                    <span>
                        <a class="share-pst" href="#" title="Share">
                            <i class="fa fa-share-alt"></i>
                        </a>
                        <ins>20</ins>
                    </span>	
                </li>
            </ul>
        </div>
    </div>
    <div class="coment-area" style="display: block;">
        <ul class="we-comet">
            @foreach ($item->comments as $value)
            <li>
                <div class="comet-avatar">
                     <img src="{{ asset('users/images/resources/'.$value->user->image_avatar) }}" alt="">
                </div>
                <div class="we-comment">
                    <h5><a href="time-line.html" title="">{{ $value->user->name}}</a></h5>
                    <p>{{ $value->content}}</p>
                </div>
            </li>
            @endforeach
            <li class="post-comment">
                <div class="comet-avatar">
                    <img src="{{ asset('users/images/resources/'.$user->image_avatar) }}" alt="">
                </div>
                <div class="post-comt-box">
                    <form method="post" name="add_comment">
                        <textarea placeholder="Post your comment" class="comment-post"  name="comment_post" id="comment_post" postId ="{{ $item->id }}"></textarea>
                        <button type="submit"></button>
                    </form>	
                </div>
            </li>
        </ul>
    </div>
</div>
@endforeach