@extends('users.auth.master')

@section('form')
<div class="we-login-register">
    <div class="form-title">
        <i class="fa fa-key"></i>Change Password
        <span>Sign Up now and meet the awesome friends around the world.</span>
    </div>
    <form class="we-form" method ="POST" name="changepassword">
        <input type="text" placeholder="uuid" name="uuid" id="uuid">
        <input type="password" placeholder="Password" name="password" id="password">
        <input type="password" placeholder="Confirm Password" name="repassword" id="repassword">
        <button type="submit" data-ripple="" class="btn-success" id="btn_changepassword">Change Password</button>
    </form>
    <span>already have an account? <a class="we-account underline" href="{{ route('user.login') }}" title="">Sign in</a></span>                    
</div>
@endsection
