@extends('users.auth.master')

@section('form')
<div class="we-login-register">
    <div class="form-title">
        <i class="fa fa-key"></i>Sign Up
        <span>Sign Up now and meet the awesome friends around the world.</span>
    </div>
    <form class="we-form" method ="POST" name="registration">
        <input type="text" placeholder="Name" name="name" id="name">
        <input type="email" placeholder="Email" name="email" id="email">
        <input type="password" placeholder="Password" name="password" id="password">
        <input type="password" placeholder="Confirm Password" name="repassword">
        <button type="submit" data-ripple="" class="btn-success" id="btn_register">Register</button>
    </form>
    <span>already have an account? <a class="we-account underline" href="{{ route('user.login') }}" title="">Sign in</a></span>                    
</div>
@endsection
