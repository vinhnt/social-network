<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title>Pitnik Social Network Toolkit</title>
    <base href="{{asset('')}}"/>
    <link rel="icon" href="users/images/fav.png" type="users/image/png" sizes="16x16"> 

    <link rel="stylesheet" href="users/css/main.min.css">
    <link rel="stylesheet" href="users/css/style.css">
    <link rel="stylesheet" href="users/css/responsive.css">

</head>
<body>
	<div class="www-layout">
        <section>
        	<div class="gap no-gap signin whitish medium-opacity">
                <div class="bg-image" style="background-image:url(users/images/resources/theme-bg.jpg)"></div>
                <div class="container">
                	<div class="row">
                        <div class="col-lg-6 pt-3">
                            <div class="mt-3">
                                <figure>
                                    <img src="users/images/logo2.png" alt="">
                                </figure>
                                <h1>Welcome to the Pitnik</h1>
                                <p>
                                    Pitnik is a social network template that can be used to connect people. use this template for multipurpose social activities like job, dating, posting, bloging and much more. Now join & Make Cool Friends around the world !!!                             
                                </p> 
                            </div>    
                        </div>
                        <div class="col-lg-6">
                            @yield('form')
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
    	<script src="users/js/main.min.js"></script>
		<script src="users/js/script.js"></script>
        <script src="users/js/register.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
        <script src="users/js/forgot.js"></script>
        <script src="users/js/updatepassword.js"></script>
        <script src="users/js/login.js"></script>
</body>	
</html>