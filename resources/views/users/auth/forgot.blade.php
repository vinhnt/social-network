@extends('users.auth.master')

@section('form')
<div class="we-login-register">
    <div class="form-title">
        <i class="fa fa-key"></i>forgot
        <span>enter your email, to send link reset password !</span>
    </div>
    <form class="we-form" method="POST" name="forgotration">
    @csrf
        <input type="email" placeholder="Email" name="email" id="email" class="email">
        <button type="submit" data-ripple="" class="btn-primary" id="btn_forgot">send</button>
    </form>
</div>
@endsection
