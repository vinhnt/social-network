@extends('users.auth.master')

@section('form')
<div class="we-login-register">
    <div class="form-title">
        <i class="fa fa-key"></i>login
        <span>sign in now and meet the awesome Friends around the world.</span>
    </div>
    <form class="we-form" method="POST" name="formlogin">
    @csrf
        <input type="email" placeholder="Email" name="email" autofocus id="email">
        <input type="password" placeholder="Password" name="password" id="password">
        <button type="submit" data-ripple="" class="btn-primary" id="btn_login">sign in</button>
        <a class="forgot underline" href="{{ route('password.request') }}" title="">forgot password?</a>
    </form>
    <span>don't have an account? <a class="we-account underline" href="{{ route('user.register') }}" title="">register now</a></span>
</div>
@endsection
