<figure>
    <div class="edit-pp">
        <label class="fileContainer">
            <i class="fa fa-camera"></i>
            <input type="file">
        </label>
    </div>
    <img src="{{ asset('users/images/resources/profile-image.jpg') }}" alt="">
    <ol class="pit-rate">
        <li class="rated"><i class="fa fa-star"></i></li>
        <li class="rated"><i class="fa fa-star"></i></li>
        <li class="rated"><i class="fa fa-star"></i></li>
        <li class="rated"><i class="fa fa-star"></i></li>
        <li class=""><i class="fa fa-star"></i></li>
        <li><span>4.7/5</span></li>
    </ol>
</figure>

<div class="profile-section">
    <div class="row">
        <div class="col-lg-2 col-md-3">
            <div class="profile-author">
                <div class="profile-author-thumb">
                    <img alt="author" src="{{ asset('users/images/resources/'.$data->image_avatar) }}">
                    <div class="edit-dp">
                        <label class="fileContainer">
                            <i class="fa fa-camera"></i>
                            <input type="file">
                        </label>
                    </div>
                </div>
                    
                <div class="author-content">
                    <a class="h4 author-name" href="{{ route('about') }}">{{ $data->name }}</a>
                </div>
            </div>
        </div>
        <div class="col-lg-10 col-md-9">
            <ul class="profile-menu">
                <li>
                    <a class="" href="{{ route('index') }}">Timeline</a>
                </li>
                <li>
                    <a class="" href="{{ route('about') }}">About</a>
                </li>
                <li>
                    <a class="" href="{{ route('friend') }}" id="friend">Friends</a>
                </li>
                <li>
                    <a class="" href="{{ route('photo') }}">Photos</a>
                </li>
            </ul>
        </div>
    </div>
</div>	