<div class="col-lg-12">
    <div class="central-meta">
        <div class="about">
            <div class="d-flex flex-row mt-2">
                <ul class="nav nav-tabs nav-tabs--vertical nav-tabs--left" >

                    <li class="nav-item">
                        <a href="#edit-profile" class="nav-link" data-toggle="tab" ><i class="fa fa-pencil"></i> Edit Profile</a>
                    </li>
                    
                </ul>
                <div class="tab-content">
                    <!-- general setting -->
                    <form method="POST" name="editprofile" enctype="multipart/form-data" action="">
                        @csrf
                        <div class="tab-pane fade" id="edit-profile" >
                            <div class="set-title">
                                <h5>Edit Profile</h5>
                                <span>People on Pitnik will get to know you with the info below</span>
                            </div>
                            <div class="setting-meta">
                                <div class="change-photo">
                                    <figure><img src="{{ asset('users/images/resources/admin2.jpg') }}" alt=""></figure>
                                    <div class="edit-img">
                                        <form class="edit-phto">
                                            
                                            <label class="fileContainer">
                                                <i class="fa fa-camera-retro"></i>
                                                Chage DP
                                            <input type="file" name="file" id="fileUpload1">
                                            </label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="stg-form-area">
                                <form method="post" class="c-form">
                                    <div>
                                        <label>Display Name</label>
                                        <input type="text" placeholder="Jack Carter" name="name" id="name">
                                    </div>
                                    <div>
                                        <button type="submit" data-ripple="" id="cancel">Cancel</button>
                                        <button type="submit" data-ripple="" id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </form>
                   <!-- edit profile -->
                    <!-- notification -->
                    <!-- messages -->
                    <!-- weather widget setting -->
                    <!-- privacy -->
                    <!-- privacy -->
                    <!-- security -->
                    <!-- apps -->
                </div>
            </div>
        </div>
    </div>	
</div>
