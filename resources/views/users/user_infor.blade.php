
<h5>{{ $data->name }}</h5>
<img src="{{ asset('users/images/resources/'.$data->image_avatar) }}" alt="">
<span class="status f-online"></span>
<div class="user-setting">
    <span class="seting-title">User setting <a href="#" title="">see all</a></span>
    <ul class="log-out">
        <li><a href="{{ asset('user/setting') }}" title=""><i class="ti-settings"></i>account setting</a></li>
        <li><a href="{{ asset('user/login') }}" title="" id="logout"><i class="ti-power-off" ></i>log out</a></li>
    </ul>
</div>                       