
<div class="central-meta">
    <div class="title-block">
        <div class="row">
            <div class="col-lg-6">
                <div class="align-left">
                    <h5>Friends / Followers <span>{{ $count }}</span></h5>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row merged20">
                </div>
            </div>
        </div>
    </div>
</div><!-- title block -->
<div class="central-meta padding30">
    <div class="row">
        @foreach ($data as $item)
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="friend-box">
                <figure>
                    <img src="{{ asset('users/images/resources/frnd-cover1.jpg') }}" alt="">
                </figure>
                <div class="frnd-meta">
                    <img src="{{ asset('users/images/resources/'.$item->image_avatar) }}" alt="">
                     <ul class="frnd-info">
                        <li><span>Name:</span> {{ $item->name }}</li>
                        <li><span>Email:</span> {{ $item->email }}</li>
                    </ul>
                    <a class="send-mesg" data-id="{{ $item->id }}">Block</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>