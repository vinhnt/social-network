@extends('users.master_layout');
@section('main')
	<section>
		<div class="gap2 gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row merged20" id="page-contents">
							@include('users/user-profile')
							@include('users/edit-profile')
						</div>
					</div>
				</div>
			</div>
		</div>	
	</section>
@endsection