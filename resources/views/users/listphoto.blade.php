<div class="central-meta">
    <div class="title-block">
        <div class="row">
            <div class="col-lg-6">
                <div class="align-left">
                    <h5>Photos <span>{{ $count }}</span></h5>
                </div>
            </div>
        </div>
    </div>
</div><!-- title block -->
<div class="central-meta">
    <div class="row merged5">
        @foreach ( $data as $item)
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <div class="item-box">
                <a class="strip" href="{{ asset('users/images/resources/'.$item->photo) }}" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                <img src="{{ asset('users/images/resources/'.$item->photo) }}" alt=""></a>
                {{-- <div class="over-photo">
                    <a href="#" title=""><i class="fa fa-heart"></i> 15</a>
                    <span>20 hours ago</span>
                </div> --}}
            </div>
        </div>
        @endforeach
        
    </div>
</div><!-- photos -->