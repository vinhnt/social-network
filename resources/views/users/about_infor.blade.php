<span class="create-post">Personal Info</span>
<div class="personal-head">
    <span class="f-title"><i class="fa fa-user"></i> About Me:</span>
    <p>
        {{ $data->name }}
    </p>
    <span class="f-title"><i class="fa fa-envelope"></i> Email</span>
    <p>
       {{ $data->email }}
    </p>
</div>									