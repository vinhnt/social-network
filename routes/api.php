<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\API\Like;
use App\Http\Controllers\API\Comments;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\API\FriendController;
use App\Http\Controllers\API\PhotoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// -------------- NO NEED AUTH -----------------------//
Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('forgot/password', 'forgetPassword');
    Route::post('update/password', 'updatePassword');
});

// --------------NEED AUTH WITH JWT -----------------------//
Route::middleware(['jwt.verify'])->group(function () {

    //AUTH LOGGED
    Route::controller(AuthController::class)->group(function () {
        Route::post('logout', 'logout');
        Route::post('refresh', 'refresh');
        Route::post('update/profile', 'updateProfile');
        Route::post('change/password', 'changePassword');
        Route::get('user/infor', 'infor');
        Route::post('user/by/id', 'getUserById');
        Route::get('user/current', 'userCurrent');
        Route::get('user/setting', 'userSetting');
        Route::get('user/about', 'about');
    });

    //INFOR USER
    Route::controller(UserController::class)->group(function () {
        Route::post('user/update/profile', 'updateProfile');
    });

    //LIKE
    Route::controller(Like::class)->group(function () {
        Route::get('like', 'getAll');
        Route::get('like/post/{id}', 'getByPostId');
        Route::post('like/user', 'getByUserId');
        Route::post('like/create', 'createLike');
        Route::get('like/unlike/{id}', 'unLike');
    });

    //COMMENT
    Route::controller(Comments::class)->group(function () {
        Route::get('comments/{id}', 'getByPostId');
        Route::post('comments/create', 'createComment');
        Route::post('comments/edit', 'editComment');
        Route::get('comments/delete/{id}', 'deleteComment');
    });

    //POST
    Route::controller(PostController::class)->group(function () {
        Route::post('/post/create', 'create');
        Route::post('/post/delete/{id}', 'delete');
        Route::post('/post/update/{id}', 'update');
        Route::get('/post/list', 'list');
    });

    //FRIEND
    Route::controller(FriendController::class)->group(function () {
        Route::post('/friend/create', 'create');
        Route::post('/friend/delete/{id}', 'delete');
        Route::get('/friend/list', 'list');
        Route::post('/friend/update/{id}/', 'update');
    });

    //PHOTO
    Route::controller(PhotoController::class)->group(function () {
        Route::get('/list/photo', 'index');
    });
});
