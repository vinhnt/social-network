<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\Admin\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//======USE AUTH WITH "web" MIDDLWARE BEEZE PAKAGE=======//
Route::middleware('auth')->group(function () {

    Route::get('/', function () {
        return view('admin.pages.index');
    })->name('dashboard');

    //INFOR USER ADMIN
    Route::controller(AdminController::class)->group(function () {
        Route::get('admin/{id}', 'allAdmin')->name('all.admin');
        Route::get('edit/profile', 'getProfileById')->name('edit.profile');
        Route::post('edit/profile', 'updateProfile')->name('profile');
        Route::get('edit/password', 'viewUpdatePassword')->name('change.password');
        Route::post('edit/password', 'updatePassword')->name('password');
    });

    //POST MANAGE
    Route::controller(PostController::class)->group(function () {
        Route::get('post', 'allPost')->name('all.post');
    });

    //USER MANAGE
    Route::controller(UserController::class)->group(function () {
        Route::get('user', 'allUser')->name('all.user');
        Route::get('user/delete/{id}', 'deleteUser')->name('user.delete');
        Route::get('user/edit/{id}', 'editUser')->name('user.edit');
    });
});

require __DIR__.'/auth.php';
