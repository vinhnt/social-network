<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//route tpl API user
Route::get('user/setting', function () {
    // return 1;
    return view('users.setting');
})->name('setting');
Route::get('user/about', function () {
    return view('users.about');
})->name('about');
Route::get('user/friend', function () {
    return view('users.friend');
})->name('friend');
Route::get('user/photo', function () {
    return view('users.photo');
})->name('photo');
Route::get('user/login', function () {
    return view('users.auth.login');
})->name('user.login');

Route::get('user/register', function () {
    return view('users.auth.register');
})->name('user.register');

Route::get('user/forgot', function () {
    return view('users.auth.forgot');
})->name('user.forgot');
Route::get('user/index', function () {
    return view('users.index.index');
})->name('index');
Route::get('user/update/password', function () {
    return view('users.auth.update_password');
});


/*
|--------------------------------------------------------------------------
| User Routes
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
